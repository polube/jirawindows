import React, { useState, useEffect } from 'react';
import Selecter from "./components/Selecter"
import './index.css'
import { getUsers, getIssues, getFields, getProjects } from './api/index'

function App() {
  const [allProjects, setProjects] = useState();
  const [allIssues, setIssues] = useState();
  const [allUsers, setUsers] = useState();
  const [allFields, setFields] = useState();
  const [projectName, setProjectName] = useState();
  const [electProject, setElectProject] = useState();

  useEffect(() => {

    getProjects()
      .then(projects => {
        setProjects(projects)
        if (!projectName) {
          setProjectName(projects[0].key)
        }
      });

    getFields().then(fields => setFields(fields));
    getUsers(projectName).then(users => setUsers(users));
    getIssues(projectName).then(users => setIssues(users));



  }, [projectName])


  useEffect(() => {

    if (allFields) {
      // eslint-disable-next-line array-callback-return
      let resOut = allFields.filter(field => {
        for (let i of Object.values(field.clauseNames)) {
          if (i === 'Responsible[People]' || i === 'Accountable[People]' || i === 'Consulted[People]' || i === 'Informed[People]') {
            return field;
          }
        }

      })
      setElectProject(resOut)
    }

  },
    [allFields],
  );

  const lol = () => {

    if (allIssues) {
      let out = allIssues.issues.filter(issue => issue.fields.filter(is => is !== null))
      console.log(out)
    }
  }

  function renderText(){
    
  }

  if (allIssues) {
    return (
      <>
        <button onClick={() => lol()}>p</button>
        <button onClick={() => console.log(electProject)}>pN</button>
        <Selecter projects={allProjects} set={setProjectName} />

        <table className="table">
          <thead>
            <tr>
              <th className="nope"></th>

              {allUsers.map(user => {
                return (
                  <th>{user.displayName}</th>
                )
              })}
            </tr>
          </thead>

          <tbody>
            {allIssues.issues.map(issue => {
              return (
                <tr>
                  <td className="firstTd">{issue.fields.summary}</td>
                  {allUsers.map(user => {
                    let result = '';

                    electProject.forEach(field => {
                      field.clauseNames.forEach(name => {

                        if (issue.fields[field.id]) {
                          if (name === 'Responsible[People]' || name === 'Accountable[People]' || name === 'Consulted[People]' || name === 'Informed[People]') {
                            if (issue.fields[field.id][0].accountId === user.accountId) {
                              result = result + name.substr(0, 1)

                            }
                          }
                        }
                      })
                    })
                    return (<td className="field">{result}</td>)

                  })}
                </tr>
              )
            })}
          </tbody>
        </table>
      </>
    )


  } else {
    return (<>
      <p>DOWNLOADING!</p>
    </>
    )
  }

}

export default App;

