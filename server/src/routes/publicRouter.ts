import express from "express";
// import addon from "../addon";

const router = express.Router();

router.get("/", async (req, res) => {
    res.redirect("/atlassian-connect.json"); // https://berlogas.atlassian.net/rest/api/2/issue/MAT-4
});


// router.get('/hello', addon.authenticate(), (req, res) => {
//     res.render('hello', {
//         title: 'MArtha rar'
//     });
// });

export default router;
